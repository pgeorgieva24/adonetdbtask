TASK:

You have a database with two tables:

Categories, which has columns CategoryId and CategoryName.
Products, which has columns ProductId, CategoryId and ProductName.
 
Your program should accept a search text and return the categories which have this text in their name, and all the products that belong to this categories.

You must use pure ADO.NET, LINQ and EF are not allowed.

Example

Input: top

Output:

Laptops
- Dell XPS
- HP Spectre
- Surface Pro
Desktop workstations
- Dell Precision
- Surface Studio


Important: You must use only one single query to the database!