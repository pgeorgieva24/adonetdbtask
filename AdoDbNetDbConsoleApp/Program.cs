using DataAccess;
using DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdoDbNetDbConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true) {
                SearchEngine searchEngine = new SearchEngine();
                Console.WriteLine("Search categories containing word: ");
                string searchText = Console.ReadLine();
                Console.WriteLine();
                List<Category> categories = searchEngine.GetCategoriesContainingWord(searchText);
                if (categories.Count() > 0)
                {
                    foreach (var category in categories)
                    {
                        Console.WriteLine(category.Name);
                        foreach (var product in category.Products)
                        {
                            Console.WriteLine("\t-" + product.Name);
                        }
                    }
                    Console.WriteLine();
                }
                else Console.WriteLine("No category contains the word " + searchText);
            }
        }
    }
}
