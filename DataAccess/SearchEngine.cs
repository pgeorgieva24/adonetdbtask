﻿using DataAccess.Models;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;

namespace DataAccess
{
    public class SearchEngine
    {
        readonly string connectionString;
        public SearchEngine()
        {
            connectionString = ConfigurationManager.ConnectionStrings["AdoNetDB"].ConnectionString;
        }

        public List<Category> GetCategoriesContainingWord(string searchWord)
        {
            List<Category> categories = new List<Category>();
            Category category = null;
            int lastCategoryId = 0;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string query = @"select ProductId, p.CategoryId, p.ProductName, c.CategoryName
                        from Product p join Category c
                        on p.CategoryId=c.CategoryId
                        where c.CategoryName like '%' + @SearchWord + '%'
                        order by p.CategoryId";

                connection.Open();

                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    SqlParameter sqlParameter = command.Parameters.AddWithValue("@SearchWord", searchWord);

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (lastCategoryId != (int)reader["CategoryId"])
                            {
                                category = new Category
                                {
                                    Id = (int)reader["CategoryId"],
                                    Name = reader["CategoryName"].ToString(),
                                    Products = new List<Product>()
                                };
                                categories.Add(category);
                                lastCategoryId = (int)reader["CategoryId"];
                            }

                            Product product = new Product
                            {
                                Id = (int)reader["ProductId"],
                                CategoryId = (int)reader["CategoryId"],
                                Name = reader["ProductName"].ToString()
                            };

                            category.Products.Add(product);
                        }

                    }
                }
            }
            return categories;
        }
    }
}
